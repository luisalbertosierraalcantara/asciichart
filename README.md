# ASCIIChart Table v1.0

Application to display ASCII characters in details.

# Screenshot

<img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgNJGmh21ioNPouBbptAYYbelQzHq5gJuExoeYQH7XJ-FicV-1xTuzVT8NS-i6ZDg4ySrxtNhEB4q7GN50F9ei0JBEPcmwkwPDjB4g_kbfGiEOk_UpRaKbahPXlbypuCmr846nn7LY2qUvWBL5AvyC2v3X6silZwHGTtMLeasaQU3EKlqoVSX8z7o5o3CTN/s645/ascii1.png">

<img src="https://blogger.googleusercontent.com/img/b/R29vZ2xl/AVvXsEgci2rG79k4nkmYktyhXb-gyfMjNhKiQmHXMEvm7zF5yaAwdv8SP94FtE20pmyUeW1UGyg6XPfcCx75YSou560dOwEYQaIRCkgrSyfeUEfBq-4HX4PmIFXVhij2qj0fRsHD9Od9ysY70itubsjUE5kqxIHsFwG1aw2QfLIDX9MBvpOhvD5T6IUGMQQOl_3f/s645/ascii2.png">



