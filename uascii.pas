unit UAscii;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls;

type

  { TFAscii }

  TFAscii = class(TForm)
    Label1: TLabel;
    Pan_10: TPanel;
    Pan_100: TPanel;
    Pan_101: TPanel;
    Pan_102: TPanel;
    Pan_103: TPanel;
    Pan_104: TPanel;
    Pan_105: TPanel;
    Pan_106: TPanel;
    Pan_107: TPanel;
    Pan_108: TPanel;
    Pan_109: TPanel;
    Pan_11: TPanel;
    Pan_110: TPanel;
    Pan_111: TPanel;
    Pan_112: TPanel;
    Pan_113: TPanel;
    Pan_114: TPanel;
    Pan_115: TPanel;
    Pan_116: TPanel;
    Pan_117: TPanel;
    Pan_118: TPanel;
    Pan_119: TPanel;
    Pan_12: TPanel;
    Pan_120: TPanel;
    Pan_121: TPanel;
    Pan_122: TPanel;
    Pan_123: TPanel;
    Pan_124: TPanel;
    Pan_125: TPanel;
    Pan_126: TPanel;
    Pan_127: TPanel;
    Pan_13: TPanel;
    Pan_14: TPanel;
    Pan_15: TPanel;
    Pan_16: TPanel;
    Pan_17: TPanel;
    Pan_18: TPanel;
    Pan_19: TPanel;
    Pan_2: TPanel;
    Pan_20: TPanel;
    Pan_21: TPanel;
    Pan_22: TPanel;
    Pan_23: TPanel;
    Pan_24: TPanel;
    Pan_25: TPanel;
    Pan_26: TPanel;
    Pan_27: TPanel;
    Pan_28: TPanel;
    Pan_29: TPanel;
    Pan_3: TPanel;
    Pan_30: TPanel;
    Pan_31: TPanel;
    Pan_32: TPanel;
    Pan_33: TPanel;
    Pan_34: TPanel;
    Pan_35: TPanel;
    Pan_36: TPanel;
    Pan_37: TPanel;
    Pan_38: TPanel;
    Pan_39: TPanel;
    Pan_4: TPanel;
    Pan_40: TPanel;
    Pan_41: TPanel;
    Pan_42: TPanel;
    Pan_43: TPanel;
    Pan_44: TPanel;
    Pan_45: TPanel;
    Pan_46: TPanel;
    Pan_47: TPanel;
    Pan_48: TPanel;
    Pan_49: TPanel;
    Pan_5: TPanel;
    Pan_50: TPanel;
    Pan_51: TPanel;
    Pan_52: TPanel;
    Pan_53: TPanel;
    Pan_54: TPanel;
    Pan_55: TPanel;
    Pan_56: TPanel;
    Pan_57: TPanel;
    Pan_58: TPanel;
    Pan_59: TPanel;
    Pan_6: TPanel;
    Pan_60: TPanel;
    Pan_61: TPanel;
    Pan_62: TPanel;
    Pan_63: TPanel;
    Pan_64: TPanel;
    Pan_65: TPanel;
    Pan_66: TPanel;
    Pan_67: TPanel;
    Pan_68: TPanel;
    Pan_69: TPanel;
    Pan_7: TPanel;
    Pan_70: TPanel;
    Pan_71: TPanel;
    Pan_72: TPanel;
    Pan_73: TPanel;
    Pan_74: TPanel;
    Pan_75: TPanel;
    Pan_76: TPanel;
    Pan_77: TPanel;
    Pan_78: TPanel;
    Pan_79: TPanel;
    Pan_8: TPanel;
    Pan_80: TPanel;
    Pan_81: TPanel;
    Pan_82: TPanel;
    Pan_83: TPanel;
    Pan_84: TPanel;
    Pan_85: TPanel;
    Pan_86: TPanel;
    Pan_87: TPanel;
    Pan_88: TPanel;
    Pan_89: TPanel;
    Pan_9: TPanel;
    Pan_90: TPanel;
    Pan_91: TPanel;
    Pan_92: TPanel;
    Pan_93: TPanel;
    Pan_94: TPanel;
    Pan_95: TPanel;
    Pan_96: TPanel;
    Pan_97: TPanel;
    Pan_98: TPanel;
    Pan_99: TPanel;
    Pan_Tittle: TPanel;
    Pan_Boddy: TPanel;
    Pan_1: TPanel;

    //Custom Proc
    procedure MouseEnterAll_proc(pan : TPanel);
    procedure MouseExitAll_proc(pan : TPanel);
    procedure MouseOver_proc(Sender: TObject);
    procedure MouseLeave_proc(Sender: TObject);

    procedure getDataClick_proc(pan : TPanel);
    procedure OnClick_proc(Sender: TObject);

    //ASCII to Number
    function Chr2Dec(ascii : String) : LongInt;

    //String to ASCII
    procedure StringToAscii(data : String);

  private

  public

  end;

var
  FAscii: TFAscii;

implementation

{$R *.lfm}

{ TFAscii }

procedure TFAscii.OnClick_proc(Sender: TObject);
begin
   getDataClick_proc(Sender as TPanel);
end;

procedure TFAscii.getDataClick_proc(pan : TPanel);
begin
  StringToAscii(pan.Caption);
end;

procedure TFAscii.MouseEnterAll_proc(pan : TPanel);
begin
  pan.Color:= StringToColor('$00FFFFC1');
end;

procedure TFAscii.MouseExitAll_proc(pan : TPanel);
begin
  pan.Color:= clMenu;
end;

procedure TFAscii.MouseLeave_proc(Sender: TObject);
begin
    MouseExitAll_proc(Sender as TPanel);
end;

procedure TFAscii.MouseOver_proc(Sender: TObject);
begin
  MouseEnterAll_proc(Sender as TPanel);
end;

procedure TFAscii.StringToAscii(data : String);
var  chr_num : LongInt;
     Res : string;
begin
        chr_num :=  Chr2Dec(data); //Dec
        chr_num.ToHexString(2); //hex
        OctStr(chr_num,3); //Oct

        Res := '--------------------------' +  Chr(13) +
               '        ASCII : '+ data + Chr(13) +
               '--------------------------' +  Chr(13) +
               'Dec: ' + chr_num.ToString + Chr(13) +
               'Hex: ' + chr_num.ToHexString(2) + Chr(13) +
               'Oct: ' + OctStr(chr_num,3) +  Chr(13) +
               '--------------------------';

        ShowMessage(Res);
end;

function TFAscii.Chr2Dec(ascii : String) : LongInt;
var character :  LongInt;
begin

  case ascii of
  'NULL' :
     begin
       character := 0;
     end;
  'SOH' :
     begin
       character := 1;
     end;
  'STX' :
     begin
       character := 2;
     end;
  'ETX' :
     begin
       character := 3;
     end;
  'EOT' :
     begin
       character := 4;
     end;
  'ENQ' : begin
       character := 5;
     end;
  'ACK' :
     begin
       character := 6;
     end;
  'BELL' :
     begin
       character := 7;
     end;
  'BS' :
     begin
       character := 8;
     end;
  'TAB' :
     begin
       character := 9;
     end;
  'LF' :
     begin
       character := 10;
     end;
  'VT' :
     begin
       character := 11;
     end;
  'FF' :
     begin
       character := 12;
     end;
  'CR' :
     begin
       character := 13;
     end;
  'SO' :
     begin
       character := 14;
     end;
  'SI' :
     begin
       character := 15;
     end;
  'DLE' :
     begin
       character := 16;
     end;
  'DC1' :
     begin
       character := 17;
     end;
  'DC2' :
     begin
       character := 18;
     end;
  'DC3' :
     begin
       character := 19;
     end;
  'DC4' :
     begin
       character := 20;
     end;
  'NAK' :
     begin
       character := 21;
     end;
  'SYN' :
     begin
       character := 22;
     end;
  'ETB' :
     begin
       character := 23;
     end;
  'CAN' :
     begin
       character := 24;
     end;
  'EM' :
     begin
       character := 25;
     end;
  'SUB' :
     begin
       character := 26;
     end;
  'ESC' :
     begin
       character := 27;
     end;
  'FS' :
     begin
       character := 28;
     end;
  'GS' :
     begin
       character := 29;
     end;
  'RS' :
     begin
       character := 30;
     end;
  'US' :
     begin
       character := 31;
     end;
  ' ' :  //Space
     begin
       character := 32;
     end;
  '!' :
     begin
       character := 33;
     end;
  '"' :
     begin
       character := 34;
     end;
  '#' :
     begin
       character := 35;
     end;
  '$' :
     begin
       character := 36;
     end;
  '%' :
     begin
       character := 37;
     end;
  '&' :
     begin
       character := 38;
     end;
  '''':
     begin
       character := 39;
     end;
  '(':
     begin
       character := 40;
     end;
  ')':
     begin
       character := 41;
     end;
  '*':
     begin
       character := 42;
     end;
  '+':
     begin
       character := 43;
     end;
  ',':
     begin
       character := 44;
     end;
  '-':
     begin
       character := 45;
     end;
  '.':
     begin
       character := 46;
     end;
  '/':
     begin
       character := 47;
     end;
  '0':
     begin
       character := 48;
     end;
  '1':
     begin
       character := 49;
     end;
  '2':
     begin
       character := 50;
     end;
  '3':
     begin
       character := 51;
     end;
  '4':
     begin
       character := 52;
     end;
  '5':
     begin
       character := 53;
     end;
  '6':
     begin
       character := 54;
     end;
  '7':
     begin
       character := 55;
     end;
  '8':
     begin
       character := 56;
     end;
  '9':
     begin
       character := 57;
     end;
  ':':
     begin
       character := 58;
     end;
  ';':
     begin
       character := 59;
     end;
  '<':
     begin
       character := 60;
     end;
  '=':
     begin
       character := 61;
     end;
  '>':
     begin
       character := 62;
     end;
  '?':
     begin
       character := 63;
     end;
  '@':
     begin
       character := 64;
     end;
  'A':
     begin
       character := 65;
     end;
  'B':
     begin
       character := 66;
     end;
  'C':
     begin
       character := 67;
     end;
  'D':
     begin
       character := 68;
     end;
  'E':
     begin
       character := 69;
     end;
  'F':
     begin
       character := 70;
     end;
  'G':
     begin
       character := 71;
     end;
  'H':
     begin
       character := 72;
     end;
  'I':
     begin
       character := 73;
     end;
  'J':
     begin
       character := 74;
     end;
  'K':
     begin
       character := 75;
     end;
  'L':
     begin
       character := 76;
     end;
  'M':
     begin
       character := 77;
     end;
  'N':
     begin
       character := 78;
     end;
  'O':
     begin
       character := 79;
     end;
  'P':
     begin
       character := 80;
     end;
  'Q':
     begin
       character := 81;
     end;
  'R':
     begin
       character := 82;
     end;
  'S':
     begin
       character := 83;
     end;
  'T':
     begin
       character := 84;
     end;
  'U':
     begin
       character := 85;
     end;
  'V':
     begin
       character := 86;
     end;
  'W':
     begin
       character := 87;
     end;
  'X':
     begin
       character := 88;
     end;
  'Y':
     begin
       character := 89;
     end;
  'Z':
     begin
       character := 90;
     end;
  '[':
     begin
       character := 91;
     end;
  '\':
     begin
       character := 92;
     end;
  ']':
     begin
       character := 93;
     end;
  '^':
     begin
       character := 94;
     end;
  '_':
     begin
       character := 95;
     end;
  '`':
     begin
       character := 96;
     end;
  'a':
     begin
       character := 97;
     end;
  'b':
     begin
       character := 98;
     end;
  'c':
     begin
       character := 99;
     end;
  'd':
     begin
       character := 100;
     end;
  'e':
     begin
       character := 101;
     end;
  'f':
     begin
       character := 102;
     end;
  'g':
     begin
       character := 103;
     end;
  'h':
     begin
       character := 104;
     end;
  'i':
     begin
       character := 105;
     end;
  'j':
     begin
       character := 106;
     end;
  'k':
     begin
       character := 107;
     end;
  'l':
     begin
       character := 108;
     end;
  'm':
     begin
       character := 109;
     end;
  'n':
     begin
       character := 110;
     end;
  'o':
     begin
       character := 111;
     end;
  'p':
     begin
       character := 112;
     end;
  'q':
     begin
       character := 113;
     end;
  'r':
     begin
       character := 114;
     end;
  's':
     begin
       character := 115;
     end;
  't':
     begin
       character := 116;
     end;
  'u':
     begin
       character := 117;
     end;
  'v':
     begin
       character := 118;
     end;
  'w':
     begin
       character := 119;
     end;
  'x':
     begin
       character := 120;
     end;
  'y':
     begin
       character := 121;
     end;
  'z':
     begin
       character := 122;
     end;
  '{':
     begin
       character := 123;
     end;
  '|':
     begin
       character := 124;
     end;
  '}':
     begin
       character := 125;
     end;
  '~':
     begin
       character := 126;
     end;
  'DEL':
     begin
       character := 127;
     end;

  end;

  Result := character;
end;

end.

